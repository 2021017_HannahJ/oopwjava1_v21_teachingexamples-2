package oojava1.m03.ex04;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileLoader {
	public static final String FILENAME = "src/oojava1/m03/ex04/FileLoader.java";
	public static int bytesLoaded = 0;
	
	public static String loadFile() {
        Path fileName = Path.of(FILENAME);
        System.out.println("Filename: "+fileName);
        File f = new File(FILENAME);
        System.out.println(f.exists() ? "File exists." : "File not found.");
        String res;
        try {
        	res = Files.readString(fileName);
        }
        catch (IOException ioe) {res = "";}
        bytesLoaded+=res.length();
        	
        return res;
    }
}